#include <iostream>
#include <vector>

#include "class.h"

using namespace std;

double PresjekPolinoma (Polinom &P, Polinom &Q, double a, double b, double e){
    Polinom funkcija;
    funkcija = P-Q;
    if (funkcija(a)*funkcija(b) > 0) throw "Polinomi nemaju presjek unutar posmatranog intervala.";
    while(fabs(b-a) > e){

        double c=(a+b)/2;
        if (funkcija(a)*funkcija(c) < 0)
            b=c;
        else if (funkcija(a)*funkcija(b) == 0)
            return a;
        else
            a=c;
    }
    return (a+b)/2;
}

bool UporedjivanjePolinoma (Polinom &P, Polinom &Q){
    if (P.stepen < Q.stepen) return true;
    else if (P.stepen > Q.stepen) return false;
    else {
        if (P.koeficijenti[P.stepen-1] > Q.koeficijenti[Q.stepen-1])  return true;
        else if (P.koeficijenti[P.stepen-1] < Q.koeficijenti[Q.stepen-1])  return false;
        else {
            int i=0;
            while (P.koeficijenti[i] == Q.koeficijenti[i]) i++;
            if (P.koeficijenti[i] < Q.koeficijenti[i])  return true;
            else  return false;
        }
    }
}

void SortPolinom (Polinom *niz, int n){
    Polinom pocetak=niz[0];
    Polinom kraj=niz[n-1];
    sort(pocetak, kraj, UporedjivanjePolinoma);
}


int main(){
    Polinom P;
    return 0;
}
