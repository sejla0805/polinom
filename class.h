#ifndef CLASS_H
#define CLASS_H
#include<iostream>
#include <vector>
#include <cmath>

using namespace std;
class Polinom {
    vector<double> koeficijenti;
    int stepen;
    double Stepenuj(double, int);
public:
    Polinom(){stepen=0;}
    Polinom(vector<double> koeficijent);
    ~Polinom(){}
    Polinom& operator=(const Polinom& p1){
        koeficijenti.clear();
        for(int i{0};  i < p1.koeficijenti.size(); i++) koeficijenti.push_back(p1.koeficijenti[i]);
        stepen = p1.stepen;
    }
    Polinom& operator=(const Polinom&& p1){
        koeficijenti.clear();
        for(int i{0};  i < p1.koeficijenti.size(); i++) koeficijenti.push_back(p1.koeficijenti[i]);
        stepen = p1.stepen;
    }
    Polinom(const Polinom& p){for(int i=0; i<p.koeficijenti.size(); i++) {koeficijenti.push_back(p.koeficijenti[i]);} stepen = koeficijenti.size() - 1;}
    Polinom(const Polinom&& p){for(int i=0; i<p.koeficijenti.size(); i++) {koeficijenti.push_back(p.koeficijenti[i]);} stepen = koeficijenti.size() - 1;}
    void umetni (double A, int n);
    friend ostream& operator<< (ostream &out, Polinom &p);
    friend istream& operator>> (istream &in, Polinom &p);
    double operator() (double x);
    friend Polinom operator+(Polinom &p1, Polinom &p2);
    friend Polinom operator-(Polinom &p1, Polinom &p2);
    friend Polinom operator*(Polinom &p1, Polinom &p2);
    friend Polinom operator^(Polinom &P, int eksponent);
    friend Polinom operator< (Polinom &P, Polinom &Q);
    double NulaPolinoma (double a, double b, double e);
    double vrijednostFunkcije (vector<double> koef, int stepen, double x);
};


#endif // CLASS_H
