#ifndef CLASS_CPP
#define CLASS_CPP
#include "class.h"
#include<iostream>
#include <cmath>
#include <vector>

using namespace std;


Polinom::Polinom(vector<double> koeficijent){
    for(int i=koeficijent.size(); i>=0; i--)
        koeficijenti.push_back(koeficijent[i]);
    stepen=koeficijent.size();
}


void Polinom::umetni(double A, int n){
    if (stepen == 0){
        int i=0;
        while (i<n){
            koeficijenti.push_back(0);
            i++;
        }
        koeficijenti.push_back(A);
    }
    else if (n < stepen){
        koeficijenti[n] += A;
    }
    else {
        int i=stepen;
        while (i < n){
            koeficijenti.push_back(0);
            i++;
        }
        koeficijenti.push_back(A);
    }
    stepen=koeficijenti.size();
}



istream& operator>> (istream &in, Polinom &p){
    char znak;
    double koef;
    int stepen;
    if (cin.peek() > '0' && cin.peek() < '9'){
        cin>>koef;
        if (cin.peek() == 'x'){
            znak=cin.get();
            if (cin.peek() == '\n'){
                stepen=1;
                p.umetni(koef, stepen);
            }
            else if (cin.peek() == '^'){
                cin.get();
                cin>>stepen;
                p.umetni(koef, stepen);
            }
            else
                throw "Nepravilan unos.";
        }
        else if (cin.peek() == '+' || cin.peek() == '-'){
            stepen=0;
            p.umetni(koef, stepen);
        }

        else
            throw "Nepravilan unos.";
    }
    else if (cin.peek() == '-'){
        cin.get();
        if (cin.peek() > '0' && cin.peek() < '9'){
            cin>>koef;
            koef=-koef;
            if (cin.peek() == 'x'){
                znak=cin.get();
                if (cin.peek() == '\n' || cin.peek() == '+' || cin.peek() == '-'){
                    stepen=1;
                    p.umetni(koef, stepen);
                }
                else if (cin.peek() == '^'){
                    cin.get();
                    cin>>stepen;
                    p.umetni(koef, stepen);
                }
                else
                    throw "Nepravilan unos.";
            }
            else if (cin.peek() == '+' || cin.peek() == '-'){
                stepen=0;
                p.umetni(koef, stepen);
            }

            else
                throw "Nepravilan unos.";
        }
    }
    else if(cin.peek() == 'x'){
        koef=1;

        if (cin.peek() == 'x'){
            znak=cin.get();
            if (cin.peek() == '\n' || cin.peek() == '+' || cin.peek() == '-'){
                stepen=1;
                p.umetni(koef, stepen);
            }
            else if (cin.peek() == '^'){
                cin.get();
                cin>>stepen;
                p.umetni(koef, stepen);
            }
            else
                throw "Nepravilan unos.";
        }
        else if (cin.peek() == '+' || cin.peek() == '-'){
            stepen=0;
            p.umetni(koef, stepen);
        }

        else
            throw "Nepravilan unos.";
    }

    cin.get(znak);

    while (znak != '\n'){

        if (cin.peek() == 'x'){
            koef=1;

            if (cin.peek() == 'x'){
                znak=cin.get();
                if (cin.peek() == '\n'){
                    stepen=1;
                    p.umetni(koef, stepen);
                }
                else if (cin.peek() == '^'){
                    cin.get();
                    cin>>stepen;
                    p.umetni(koef, stepen);
                }
                else
                    throw "Nepravilan unos.";
            }
            else if (cin.peek() == '+' || cin.peek() == '-' || cin.peek() == '\n'){
                stepen=0;
                p.umetni(koef, stepen);
            }

            else
                throw "Nepravilan unos.";
        }
        else if (cin.peek() == '-'){
            cin.get();
            if (cin.peek() == 'x'){
                koef=-1;

                if (cin.peek() == 'x'){
                    znak=cin.get();
                    if (cin.peek() == '\n'){
                        stepen=1;
                        p.umetni(koef, stepen);
                    }
                    else if (cin.peek() == '^'){
                        cin.get();
                        cin>>stepen;
                        p.umetni(koef, stepen);
                    }
                    else
                        throw "Nepravilan unos.";
                }
                else if (cin.peek() == '+' || cin.peek() == '-' || cin.peek() == '\n'){
                    stepen=0;
                    p.umetni(koef, stepen);
                }

                else
                    throw "Nepravilan unos.";
            }
            else {
                cin>>koef;
                koef=-koef;

                if (!cin) throw "Neispravan unos.";

                if (cin.peek() == 'x'){
                    znak=cin.get();
                    if (cin.peek() == '\n' || cin.peek() == '+' || cin.peek() == '-'){
                        stepen=1;
                        p.umetni(koef, stepen);
                    }
                    else if (cin.peek() == '^' || cin.peek() == '+' || cin.peek() == '-'){
                        cin.get();
                        cin>>stepen;
                        p.umetni(koef, stepen);
                    }
                    else
                        throw "Nepravilan unos.";
                }
                else if (cin.peek() == '+' || cin.peek() == '-' || cin.peek() == '\n'){
                    stepen=0;
                    p.umetni(koef, stepen);
                }
            }
        }
        else {
            cin>>koef;

            if (!cin) throw "Neispravan unos.";

            if (cin.peek() == 'x'){
                znak=cin.get();
                if (cin.peek() == '\n' || cin.peek() == '+' || cin.peek() == '-'){
                    stepen=1;
                    p.umetni(koef, stepen);
                }
                else if (cin.peek() == '^'){
                    cin.get();
                    cin>>stepen;
                    p.umetni(koef, stepen);
                }
                else
                    throw "Nepravilan unos.";
            }
            else if (cin.peek() == '+' || cin.peek() == '-' || cin.peek() == '\n'){
                stepen=0;
                p.umetni(koef, stepen);
            }
        }
        znak=cin.peek();

    }
    return in;
}


ostream& operator<<(ostream &out, Polinom &p){
    int n=p.stepen-1;

    if (p.koeficijenti[n] > 0 && p.koeficijenti[n] == 1)
        out<<"x^"<<n;

    else if (p.koeficijenti[n] > 0)
        out<<p.koeficijenti[n]<<"x^"<<n;

    else if (p.koeficijenti[n] < 0 && p.koeficijenti[n] == -1)
        out<<"- x^"<<n;

    else if (p.koeficijenti[n] < 0)
        out<<" - "<<fabs(p.koeficijenti[n])<<"x^"<<n;

    else {}

    for (int i=n-1; i>=2; i--){
        if (p.koeficijenti[i] > 0 && p.koeficijenti[i] == 1)
            out<<" + x^"<<i;

        else if (p.koeficijenti[i] > 0)
            out<<" + "<<p.koeficijenti[i]<<"x^"<<i;

        else if (p.koeficijenti[i] < 0 && p.koeficijenti[i] == -1)
            out<<" - "<<p.koeficijenti[i]<<"x^"<<i;

        else if (p.koeficijenti[i] < 0)
            out<<" - "<<fabs(p.koeficijenti[i])<<"x^"<<i;

        else {}
    }


    if (p.koeficijenti[1] > 0 && p.koeficijenti[1] == 1)
        out<<" + x";
    else if (p.koeficijenti[1] > 0)
        out<<" + "<<p.koeficijenti[1]<<"x";
    else if (p.koeficijenti[1] < 0 && p.koeficijenti[1] == -1)
        out<<" - x";
    else if (p.koeficijenti[1] < 0)
        out<<" - "<<fabs(p.koeficijenti[1])<<"x";
    else {}


    if (p.koeficijenti[0] > 0)
        out<<" + "<<p.koeficijenti[0];
    else if (p.koeficijenti[0] < 0)
        out<<" - "<<fabs(p.koeficijenti[0]);
    else {}

    return out;
}

double Polinom::Stepenuj (double x, int n){
    double rez;
    double suma=0;
    if (n==0)
        return 1;
    if (n==1)
        return x;
    rez=Stepenuj(x, n/2);
    if (n%2==0)
        return rez*rez;
    else
        return rez*rez*x;
}

double Polinom::operator() (double x){
    double suma=0;
    int n=stepen;
    for (int i=0; i<n; i++){
        suma += koeficijenti[i]*Stepenuj(x, i);
    }
    return suma;
}


Polinom operator-(Polinom &P, Polinom &Q){
    Polinom rez;
    int pstepen = P.stepen-1;

    int qstepen =Q.stepen-1;
    if (pstepen >= qstepen){

        for (int i=0; i<=qstepen; i++){
            rez.koeficijenti.push_back(P.koeficijenti[i] - Q.koeficijenti[i]);

        }
        for(int i =qstepen+1; i < pstepen; i++){
            rez.koeficijenti.push_back(P.koeficijenti[i]);

        }

        rez.stepen=rez.koeficijenti.size();
    }
    else {
        for (int i=0; i<=qstepen; i++){
            rez.koeficijenti.push_back(P.koeficijenti[i] - Q.koeficijenti[i]);

        }
        for(int i = qstepen+1; i < pstepen; i++){
            rez.koeficijenti.push_back(P.koeficijenti[i]);

        }
        rez.stepen=rez.koeficijenti.size()-1;
    }

    return rez;
}


Polinom operator+(Polinom& P, Polinom& Q){
    Polinom rez;
    int pstepen=P.stepen-1;
    int qstepen=Q.stepen-1;
    if (pstepen >= qstepen){

        for (int i=0; i<=qstepen; i++)
            rez.koeficijenti.push_back(P.koeficijenti[i] + Q.koeficijenti[i]);


        for (int i=qstepen; i<=pstepen; i++)
            rez.koeficijenti.push_back(P.koeficijenti[i]);
    }

    else {

        for (int i=0; i<=qstepen; i++)
            rez.koeficijenti.push_back(P.koeficijenti[i] + Q.koeficijenti[i]);

        for (int i=pstepen; i<=qstepen; i++)
            rez.koeficijenti.push_back(Q.koeficijenti[i]);
    }

    rez.stepen=rez.koeficijenti.size();
    return rez;
}


Polinom operator*(Polinom &p1, Polinom &p2){
    Polinom rjesenje;

    if (p1.stepen >= p2.stepen){
        for (int i=0; i<p1.stepen; i++){
            for (int j=0; j<p2.stepen; j++){
                rjesenje.umetni(p1.koeficijenti[i]*p2.koeficijenti[j], i+j);
            }
        }
    }
    else {
        for (int i=0; i<p2.stepen; i++){
            for (int j=0; j<p1.stepen; j++)
                rjesenje.umetni(p2.koeficijenti[i]*p1.koeficijenti[j], i+j);
        }
    }
    return rjesenje;
}


Polinom operator^ (Polinom &P, int eksponent){
    Polinom stepenovani;
    stepenovani=P;
    Polinom rjesenje;
    for (int i=1; i<floor(eksponent/2); i++){
        stepenovani = stepenovani*P;
    }
    if (eksponent%2 == 0)
        rjesenje = stepenovani*stepenovani;
    else{
        rjesenje = stepenovani*stepenovani;
        rjesenje = rjesenje*P;
    }
    return rjesenje;

}


double Polinom::vrijednostFunkcije (vector<double> koef, int stepen, double x){
    double suma=0;
    for (int i=0; i<stepen; i++)
        suma = suma + koef[i]*pow(x, i);
    return suma;
}

double Polinom::NulaPolinoma (double a, double b, double e){

    double A=vrijednostFunkcije(koeficijenti, stepen, a);
    double B=vrijednostFunkcije(koeficijenti, stepen, b);
    double C=0;
    if (A * B > 0) throw "Kriva na rubovima je istog znaka, pa je nula ili parna ili parne visetrukosti.";
    int n=int(log(b-a/e)-1);
    for (int i=0; i<n; i++){
        double c=(a+b)/2;
        C=vrijednostFunkcije(koeficijenti, stepen, c);
        if (A * c< 0)
            b=c;
        else if (A * C == 0)
            return a;
        else
            a=c;
    }
    return (a+b)/2;
}


#endif // CLASS_CPP
